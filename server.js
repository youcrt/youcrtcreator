#!/usr/bin/env node

var express = require('express');
var app = express();
var port = 8888;
var server = app.listen(port, function () {
  console.log('YouCRTCreator app listening on port ' + port)
})

app.use(express.static('./public'));
